GDPC                 �                                                                         \   res://.godot/exported/133200997/export-9dcc6bb88625849bf2db6a066ed102a1-Triangle_Line.scn   �      �      ��ԣ�l�J����8    P   res://.godot/exported/133200997/export-a8e1520d83f25710dd4e3da8c6f32601-Main.scn�      A      %�����
� ����m�    X   res://.godot/exported/133200997/export-bd5a93b9780004a24f8ae9fb9b86c16f-Site_Line.scn          y      J�{4��0�M��ȝ8    ,   res://.godot/global_script_class_cache.cfg          �      r�*\��e���TWÏ    D   res://.godot/imported/icon.svg-218a8f2b3041327d8a5756f3a245f83b.ctex�>      \      6(4�d=EQ�ǮVj,       res://.godot/uid_cache.bin  �^      �       �r�ޢ�2���/`��       res://Scenes/Main.tscn.remap M      a       �u��.]�V����    $   res://Scenes/Site_Line.tscn.remap   pM      f       0��9�����b+�M.:�    (   res://Scenes/Triangle_Line.tscn.remap   �M      j       �� l�O���\\:J    (   res://Scripts/Common/Delaunay_Bower.gd        �'      ���r�P�����D       res://Scripts/Main/Main.gd  �2      �      ��ɶ�lc�Y2����N       res://Scripts/Site_Line.gd  p>      $       Y���D����ߤc՘�        res://Scripts/Triangle_Line.gd  �>      (       ����!\�3��Z��       res://icon.svg  PN      N      ]��s�9^w/�����       res://icon.svg.import   0L      �       �A��}�ZW���Mp�       res://project.binary@_      M      ��J�6悳"�7��    �8���Bplist=Array[Dictionary]([{
"base": &"RefCounted",
"class": &"Delaunay_Bower",
"icon": "",
"language": &"GDScript",
"path": "res://Scripts/Common/Delaunay_Bower.gd"
}, {
"base": &"Line2D",
"class": &"Site_Line",
"icon": "",
"language": &"GDScript",
"path": "res://Scripts/Site_Line.gd"
}, {
"base": &"Line2D",
"class": &"Triangle_Line",
"icon": "",
"language": &"GDScript",
"path": "res://Scripts/Triangle_Line.gd"
}])
�D���G�y�=,�"�RSRC                     PackedScene            ��������                                                  resource_local_to_scene    resource_name 	   _bundled    script       Script    res://Scripts/Main/Main.gd ��������      local://PackedScene_gc6d2          PackedScene          	         names "         Main    script    Node2D    Button    offset_left    offset_top    offset_right    offset_bottom    text 	   LineEdit    _on_button_pressed    pressed    _on_line_edit_text_changed    text_changed    	   variants    	                   A     %C     4B      Триангулировать      8C     @A   �{C     ,B      node_count             nodes     )   ��������       ����                            ����                                             	   	   ����                                     conn_count             conns                  
                                            node_paths              editable_instances              version             RSRC�C.�x`o$���qRSRC                     PackedScene            ��������                                                  resource_local_to_scene    resource_name 	   _bundled    script       Script    res://Scripts/Site_Line.gd ��������      local://PackedScene_cxmmj          PackedScene          	         names "      
   Site_Line    script    Line2D    	   variants                       node_count             nodes     	   ��������       ����                    conn_count              conns               node_paths              editable_instances              version             RSRC_�k�n�9RSRC                     PackedScene            ��������                                                  resource_local_to_scene    resource_name 	   _bundled    script       Script    res://Scripts/Triangle_Line.gd ��������      local://PackedScene_ptvkv          PackedScene          	         names "         Triangle_Line    script    Line2D    	   variants                       node_count             nodes     	   ��������       ����                    conn_count              conns               node_paths              editable_instances              version             RSRC�8��fC�����?# Триангуляция Делоне согласно алгоритму Боуэра-Ватсона
# со сложностью O(n^2)
# https://ru.wikipedia.org/wiki/Алгоритм_Боуэра_—_Ватсона
# https://en.wikipedia.org/wiki/Bowyer–Watson_algorithm
class_name Delaunay_Bower

# ==== Классы ====

# Ребро
class Edge:
	var a: Vector2
	var b: Vector2
	
	func _init(a_outer: Vector2, b_outer: Vector2) ->void:
		self.a = a_outer
		self.b = b_outer
		
	func equals(edge: Edge) -> bool:
		return (a == edge.a && b == edge.b) || (a == edge.b && b == edge.a)
		
	func length() -> float:
		return a.distance_to(b)
		
	func center() -> Vector2:
		return (a + b) / 2


# Треугольник Делоне
class Triangle:
	var a: Vector2
	var b: Vector2
	var c: Vector2
	
	var edge_ab: Edge
	var edge_bc: Edge
	var edge_ca: Edge
	
	var center: Vector2
	var radius_sqr: float
	
	func _init(a_outer: Vector2, b_outer: Vector2, c_outer: Vector2) ->void:
		self.a = a_outer
		self.b = b_outer
		self.c = c_outer
		edge_ab = Edge.new(a_outer,b_outer)
		edge_bc = Edge.new(b_outer,c_outer)
		edge_ca = Edge.new(c_outer,a_outer)
		recalculate_circumcircle()
	
	
	# пересчитывает окружность
	func recalculate_circumcircle() -> void:
		var ab = a.length_squared()
		var cd = b.length_squared()
		var ef = c.length_squared()
		
		var cmb = c - b
		var amc = a - c
		var bma = b - a
		
		var circum = Vector2(
			(ab * cmb.y + cd * amc.y + ef * bma.y) / (a.x * cmb.y + b.x * amc.y + c.x * bma.y),
			(ab * cmb.x + cd * amc.x + ef * bma.x) / (a.y * cmb.x + b.y * amc.x + c.y * bma.x)
		)
		
		center = circum * 0.5
		radius_sqr = a.distance_squared_to(center)
	
	
	func is_point_inside_circumcircle(point: Vector2) -> bool:
		return center.distance_squared_to(point) < radius_sqr
	
	
	func is_corner(point: Vector2) -> bool:
		return point == a || point == b || point == c
	
	
	func get_corner_opposite_edge(corner: Vector2) -> Edge:
		if corner == a:
			return edge_bc
		elif corner == b:
			return edge_ca
		elif corner == c:
			return edge_ab
		else:
			return null


# Сайт - центральная точка, вокруг которой строится локус
class VoronoiSite:
	var id: int
	var center: Vector2
	var polygon: PackedVector2Array # Точки, расположенные по часовой стрелке
	var source_triangles: Array # Массив треугольников, создающих массив рёбер вороного
	var neightbours: Array # Соседи
	
	
	func _init(center_outer: Vector2):
		self.center = center_outer
	
	
	func _sort_source_triangles(a: Triangle, b: Triangle) -> bool:
		var da = center.direction_to(a.center).angle()
		var db = center.direction_to(b.center).angle()
		return da < db # сортировка по часовой стрелке
	
	
	func get_boundary() -> Rect2:
		var rect = Rect2(polygon[0], Vector2.ZERO)
		for point in polygon:
			rect = rect.expand(point)
		return rect


# Ребро локуса
class VoronoiEdge:
	var a: Vector2
	var b: Vector2
	var this: VoronoiSite
	var other: VoronoiSite
	
	func equals(edge: VoronoiEdge) -> bool:
		return (a == edge.a && b == edge.b) || (a == edge.b && b == edge.a)
		
	func length() -> float:
		return a.distance_to(b)
		
	func center() -> Vector2:
		return (a + b) * 0.5
		
	func normal() -> Vector2:
		return a.direction_to(b).orthogonal()


# Вычисляет прямоугольник, содержащий все заданные точки
static func calculate_rect(points_outer: PackedVector2Array) -> Rect2:
	var rect = Rect2(points_outer[0], Vector2.ZERO)
	for point in points_outer:
		rect = rect.expand(point)
	return rect.grow(0)


var points: PackedVector2Array
var _rect: Rect2
var _rect_super: Rect2
var _rect_super_corners: Array
var _rect_super_triangle1: Triangle
var _rect_super_triangle2: Triangle


# ==== Конструктор ====
func _init(rect: Rect2 = Rect2()) ->void:
	set_rectangle(rect)


# ==== Публичные функции ====

func add_point(point: Vector2):
	points.append(point)


func sort_points() ->void:
	points.sort()


func set_rectangle(rect: Rect2) -> void:
	_rect = rect # сохраняем исходный прямоугольник
	
	# расширяем прямоугольник до родительского прямоугольника, чтобы убедиться, что
	# все будущие точки не будут слишком близко к границам
	var rect_max_size = max(_rect.size.x, _rect.size.y)
	_rect_super = _rect.grow(rect_max_size)
	
	# вычисляем и кэшируем треугольники для род. прямоугольника
	var c0 = Vector2(_rect_super.position)
	var c1 = Vector2(_rect_super.position + Vector2(_rect_super.size.x,0))
	var c2 = Vector2(_rect_super.position + Vector2(0,_rect_super.size.y))
	var c3 = Vector2(_rect_super.end)
	_rect_super_corners.append_array([c0,c1,c2,c3])
	_rect_super_triangle1 = Triangle.new(c0,c1,c2)
	_rect_super_triangle2 = Triangle.new(c1,c2,c3)


func is_border_triangle(triangle: Triangle) -> bool:
	return _rect_super_corners.has(triangle.a) || _rect_super_corners.has(triangle.b) || _rect_super_corners.has(triangle.c)


func remove_border_triangles(triangulation: Array) -> void:
	var border_triangles: Array = []
	for triangle in triangulation:
		if is_border_triangle(triangle):
			border_triangles.append(triangle)
	for border_triangle in border_triangles:
		triangulation.erase(border_triangle)


func is_border_site(site: VoronoiSite) -> bool:
	return !_rect.encloses(site.get_boundary())


func remove_border_sites(sites: Array) -> void:
	var border_sites: Array = []
	for site in sites:
		if is_border_site(site):
			border_sites.append(site)
	for border_site in border_sites:
		sites.erase(border_site)


func triangulate() -> Array: # Массив треугольников
	var triangulation: Array = [] # Массив треугольников
	# Считаем прямоугольники, если у них нет площади
	if !(_rect.has_area()):
		set_rectangle(Delaunay_Bower.calculate_rect(points))
	triangulation.append(_rect_super_triangle1)
	triangulation.append(_rect_super_triangle2)
	var bad_triangles: Array = [] # Массив треугольников
	var polygon: Array = [] # Массив рёбер
	for point in points:
		bad_triangles.clear()
		polygon.clear()
		_find_bad_triangles(point, triangulation, bad_triangles)
		for bad_tirangle in bad_triangles:
			triangulation.erase(bad_tirangle)
		_make_outer_polygon(bad_triangles, polygon)
		for edge in polygon:
			triangulation.append(Triangle.new(point, edge.a, edge.b))
	return triangulation


func make_voronoi(triangulation: Array) -> Dictionary: # Массив ребер вороного
	var sites: Dictionary = {}
	var completion_counter: Array = []
	var triangle_usage: Dictionary = {} # Словарь {треугольник => [сайты]}, используемый в поиске соседей
	for triangle in triangulation:
		triangle_usage[triangle] = []
	var id_inc: int = 0
	for point in points:
		var site = VoronoiSite.new(point)
		site.id = id_inc
		id_inc += 1
		completion_counter.clear()
		for triangle in triangulation:
			if !triangle.is_corner(point):
				continue
			site.source_triangles.append(triangle)
			var edge = triangle.get_corner_opposite_edge(point)
			completion_counter.erase(edge.a)
			completion_counter.erase(edge.b)
			completion_counter.append(edge.a)
			completion_counter.append(edge.b)
		if completion_counter.size() != site.source_triangles.size():
			continue
		site.source_triangles.sort_custom(Callable(site, "_sort_source_triangles"))
		var polygon: PackedVector2Array = []
		for triangle in site.source_triangles:
			# Добавляем к полигону сайта центр треугольника из триангуляции
			polygon.append(triangle.center)
			triangle_usage[triangle].append(site)
		site.polygon = polygon
		sites[site.id] = site
	# ищем соседей
	for site in sites.values():
		for triangle in site.source_triangles:
			var posibilities = triangle_usage[triangle]
			var neightbour = _find_voronoi_neightbour(site, triangle, posibilities)
			if neightbour != null:
				site.neightbours.append(neightbour)
	
	return sites # рёбра


# ==== ПРИВАТНЫЕ ФУНКЦИИ ====

func _make_outer_polygon(triangles: Array, out_polygon: Array) -> void:
	var duplicates: Array = [] # массив рёбер
	
	for triangle in triangles:
		out_polygon.append(triangle.edge_ab)
		out_polygon.append(triangle.edge_bc)
		out_polygon.append(triangle.edge_ca)
		
	for edge1 in out_polygon:
		for edge2 in out_polygon:
			if edge1 != edge2 && edge1.equals(edge2):
				duplicates.append(edge1)
				duplicates.append(edge2)
				
	for edge in duplicates:
		out_polygon.erase(edge)


# ищет плохие треугольники 
func _find_bad_triangles(point: Vector2, triangles: Array, out_bad_triangles: Array) -> void:
	for triangle in triangles:
		if triangle.is_point_inside_circumcircle(point):
			out_bad_triangles.append(triangle)


# ищет соседей
func _find_voronoi_neightbour(site: VoronoiSite, triangle: Triangle, possibilities: Array) -> VoronoiEdge:
	var triangle_index = site.source_triangles.find(triangle)
	var next_triangle_index = triangle_index + 1
	if (next_triangle_index == site.source_triangles.size()):
		next_triangle_index = 0
	var next_triangle = site.source_triangles[next_triangle_index]
	
	var opposite_edge = triangle.get_corner_opposite_edge(site.center)
	var opposite_edge_next = next_triangle.get_corner_opposite_edge(site.center)
	var common_point = opposite_edge.a
	if common_point != opposite_edge_next.a && common_point != opposite_edge_next.b:
		common_point = opposite_edge.b
		
	for pos_site in possibilities:
		if pos_site.center != common_point:
			continue
		
		var edge = VoronoiEdge.new()
		edge.a = triangle.center
		edge.b = next_triangle.center
		edge.this = site
		edge.other = pos_site
		return edge
		
	return null
HR;nI��qextends Node2D
var voronoi:Delaunay_Bower
var triangulars:Array
var current_state:STATE = STATE.START
var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y
var ranges:Dictionary = {
	"map_range":20
}
enum STATE{
	START,
	TRIANGULATE,
	VORONOI
}

# Called when the node enters the scene tree for the first time.
func _ready() ->void:
	$Button.text = "Триангулировать"
	current_state = STATE.START
	voronoi = Delaunay_Bower.new()
	
	for _i in range(ranges.map_range):
		voronoi.add_point(
			Vector2(
				randf_range(0,screen_size_x),
				randf_range(0,screen_size_y)))
	
	voronoi.sort_points()
	show_points()


func show_points() ->void:
	for point in voronoi.points:
		show_centroid(point)


func show_centroid(point: Vector2) ->void:
	var polygon = Polygon2D.new()
	var p = PackedVector2Array()
	var s = 4
	p.append(Vector2(-s,s))
	p.append(Vector2(s,s))
	p.append(Vector2(s,-s))
	p.append(Vector2(-s,-s))
	polygon.polygon = p
	polygon.color = Color.BLACK
	polygon.position = point
	add_child(polygon)


func show_site(site: Delaunay_Bower.VoronoiSite):
	var line = Site_Line.new()
	var p = site.polygon
	p.append(p[0])
	line.points = p
	line.width = 3
	line.default_color = Color.RED
	add_child(line)


func show_triangle(t: Delaunay_Bower.Triangle):
	var line = Triangle_Line.new()
	line.points = [t.a,t.b,t.c,t.a]
	line.width = 3
	line.default_color = Color.RED
	add_child(line)


func recolor_triangle_lines():
	for child in get_children():
		if child is Triangle_Line:
			child.width = 1
			child.default_color = Color.BLACK


func remove_points() ->void:
	for child in get_children():
		if child is Polygon2D:
			child.queue_free()


func remove_triangles() ->void:
	for child in get_children():
		if child is Triangle_Line:
			child.queue_free()


func remove_sites() ->void:
	for child in get_children():
		if child is Site_Line:
			child.queue_free()


func triangulate() ->void:
	triangulars = voronoi.triangulate()
	for t in triangulars:
		show_triangle(t)
	$Button.text = "Построить диаграмму Вороного"
	current_state = STATE.TRIANGULATE


func make_voronoi() ->void:
	var v: Dictionary = voronoi.make_voronoi(triangulars)
	for site_i in range(v.size()):
		var site = v[site_i] as Delaunay_Bower.VoronoiSite
		recolor_triangle_lines()
		show_site(site)
	$Button.text = "Сгенерировать новые точки"
	current_state = STATE.VORONOI


func _on_button_pressed() ->void:
	match current_state:
		STATE.START:
			triangulate()
			$LineEdit.hide()
			return
		STATE.TRIANGULATE:
			make_voronoi()
			$LineEdit.hide()
			return
		STATE.VORONOI:
			remove_points()
			remove_triangles()
			remove_sites()
			_ready()
			$LineEdit.text = str(ranges.map_range)
			$LineEdit.show()
			return


func _on_line_edit_text_changed(new_text):
	if(current_state == STATE.START):
		var range = int(new_text)
		if(range>2 && range < 500):
			ranges.map_range = range
			remove_points()
			_ready()
g�BKclass_name Site_Line
extends Line2D
u�&x-zO{~q1class_name Triangle_Line
extends Line2D
7�1��OWGST2   �   �      ����               � �        $  RIFF  WEBPVP8L  /������!"2�H�l�m�l�H�Q/H^��޷������d��g�(9�$E�Z��ߓ���'3���ض�U�j��$�՜ʝI۶c��3� [���5v�ɶ�=�Ԯ�m���mG�����j�m�m�_�XV����r*snZ'eS�����]n�w�Z:G9�>B�m�It��R#�^�6��($Ɓm+q�h��6�5��I��'���F�"ɹ{�p����	"+d������M�q��� .^>и��6��a�q��gD�h:L��A�\D�
�\=k�(���_d2W��dV#w�o�	����I]�q5�*$8Ѡ$G9��lH]��c�LX���ZӞ3֌����r�2�2ؽL��d��l��1�.��u�5�!�]2�E��`+�H&�T�D�ި7P��&I�<�ng5�O!��͙������n�
ؚѠ:��W��J�+�����6��ɒ�HD�S�z�����8�&�kF�j7N�;YK�$R�����]�VٶHm���)�rh+���ɮ�d�Q��
����]	SU�9�B��fQm]��~Z�x.�/��2q���R���,snV{�7c,���p�I�kSg�[cNP=��b���74pf��)w<:Ŋ��7j0���{4�R_��K܅1�����<߁����Vs)�j�c8���L�Um% �*m/�*+ �<����S��ɩ��a��?��F�w��"�`���BrV�����4�1�*��F^���IKJg`��MK������!iVem�9�IN3;#cL��n/�i����q+������trʈkJI-����R��H�O�ܕ����2
���1&���v�ֳ+Q4蝁U
���m�c�����v% J!��+��v%�M�Z��ꚺ���0N��Q2�9e�qä�U��ZL��䜁�u_(���I؛j+0Ɩ�Z��}�s*�]���Kܙ����SG��+�3p�Ei�,n&���>lgC���!qյ�(_e����2ic3iڦ�U��j�q�RsUi����)w��Rt�=c,if:2ɛ�1�6I�����^^UVx*e��1�8%��DzR1�R'u]Q�	�rs��]���"���lW���a7]o�����~P���W��lZ�+��>�j^c�+a��4���jDNὗ�-��8'n�?e��hҴ�iA�QH)J�R�D�̰oX?ؿ�i�#�?����g�к�@�e�=C{���&��ށ�+ڕ��|h\��'Ч_G�F��U^u2T��ӁQ%�e|���p ���������k	V����eq3���8 � �K�w|�Ɗ����oz-V���s ����"�H%* �	z��xl�4��u�"�Hk�L��P���i��������0�H!�g�Ɲ&��|bn�������]4�"}�"���9;K���s��"c. 8�6�&��N3R"p�pI}��*G��3@�`��ok}��9?"@<�����sv� ���Ԣ��Q@,�A��P8��2��B��r��X��3�= n$�^ ������<ܽ�r"�1 �^��i��a �(��~dp-V�rB�eB8��]
�p ZA$\3U�~B�O ��;��-|��
{�V��6���o��D��D0\R��k����8��!�I�-���-<��/<JhN��W�1���H�#2:E(*�H���{��>��&!��$| �~�\#��8�> �H??�	E#��VY���t7���> 6�"�&ZJ��p�C_j���	P:�a�G0 �J��$�M���@�Q��[z��i��~q�1?�E�p�������7i���<*�,b�е���Z����N-
��>/.�g�'R�e��K�)"}��K�U�ƹ�>��#�rw߶ȑqF�l�Ο�ͧ�e�3�[䴻o�L~���EN�N�U9�������w��G����B���t��~�����qk-ί�#��Ξ����κ���Z��u����;{�ȴ<������N�~���hA+�r ���/����~o�9>3.3�s������}^^�_�����8���S@s%��]K�\�)��B�w�Uc۽��X�ǧ�;M<*)�ȝ&����~$#%�q����������Q�4ytz�S]�Y9���ޡ$-5���.���S_��?�O/���]�7�;��L��Zb�����8���Guo�[''�،E%���;����#Ɲ&f��_1�߃fw�!E�BX���v��+�p�DjG��j�4�G�Wr����� 3�7��� ������(����"=�NY!<l��	mr�՚���Jk�mpga�;��\)6�*k�'b�;	�V^ݨ�mN�f�S���ն�a���ϡq�[f|#U����^����jO/���9͑Z��������.ɫ�/���������I�D��R�8�5��+��H4�N����J��l�'�כ�������H����%"��Z�� ����`"��̃��L���>ij~Z,qWXo�}{�y�i�G���sz�Q�?�����������lZdF?�]FXm�-r�m����Ф:�З���:}|x���>e������{�0���v��Gş�������u{�^��}hR���f�"����2�:=��)�X\[���Ů=�Qg��Y&�q�6-,P3�{�vI�}��f��}�~��r�r�k�8�{���υ����O�֌ӹ�/�>�}�t	��|���Úq&���ݟW����ᓟwk�9���c̊l��Ui�̸~��f��i���_�j�S-|��w�R�<Lծd�ٞ,9�8��I�Ү�6 *3�ey�[�Ԗ�k��R���<������
g�R���~��a��
��ʾiI9u����*ۏ�ü�<mԤ���T��Amf�B���ĝq��iS��4��yqm-w�j��̝qc�3�j�˝mqm]P��4���8i�d�u�΄ݿ���X���KG.�?l�<�����!��Z�V�\8��ʡ�@����mK�l���p0/$R�����X�	Z��B�]=Vq �R�bk�U�r�[�� ���d�9-�:g I<2�Oy�k���������H�8����Z�<t��A�i��#�ӧ0"�m�:X�1X���GҖ@n�I�겦�CM��@������G"f���A$�t�oyJ{θxOi�-7�F�n"�eS����=ɞ���A��Aq�V��e����↨�����U3�c�*�*44C��V�:4�ĳ%�xr2V�_)^�a]\dZEZ�C 
�*V#��	NP��\�(�4^sh8T�H��P�_��}���[remap]

importer="texture"
type="CompressedTexture2D"
uid="uid://daeusj0o4q2en"
path="res://.godot/imported/icon.svg-218a8f2b3041327d8a5756f3a245f83b.ctex"
metadata={
"vram_texture": false
}
 ڽ��J�P}���[remap]

path="res://.godot/exported/133200997/export-a8e1520d83f25710dd4e3da8c6f32601-Main.scn"
A%7۽��q�.�_[remap]

path="res://.godot/exported/133200997/export-bd5a93b9780004a24f8ae9fb9b86c16f-Site_Line.scn"
����񥗠�[remap]

path="res://.godot/exported/133200997/export-9dcc6bb88625849bf2db6a066ed102a1-Triangle_Line.scn"
��ު<svg height="128" width="128" xmlns="http://www.w3.org/2000/svg"><g transform="translate(32 32)"><path d="m-16-32c-8.86 0-16 7.13-16 15.99v95.98c0 8.86 7.13 15.99 16 15.99h96c8.86 0 16-7.13 16-15.99v-95.98c0-8.85-7.14-15.99-16-15.99z" fill="#363d52"/><path d="m-16-32c-8.86 0-16 7.13-16 15.99v95.98c0 8.86 7.13 15.99 16 15.99h96c8.86 0 16-7.13 16-15.99v-95.98c0-8.85-7.14-15.99-16-15.99zm0 4h96c6.64 0 12 5.35 12 11.99v95.98c0 6.64-5.35 11.99-12 11.99h-96c-6.64 0-12-5.35-12-11.99v-95.98c0-6.64 5.36-11.99 12-11.99z" fill-opacity=".4"/></g><g stroke-width="9.92746" transform="matrix(.10073078 0 0 .10073078 12.425923 2.256365)"><path d="m0 0s-.325 1.994-.515 1.976l-36.182-3.491c-2.879-.278-5.115-2.574-5.317-5.459l-.994-14.247-27.992-1.997-1.904 12.912c-.424 2.872-2.932 5.037-5.835 5.037h-38.188c-2.902 0-5.41-2.165-5.834-5.037l-1.905-12.912-27.992 1.997-.994 14.247c-.202 2.886-2.438 5.182-5.317 5.46l-36.2 3.49c-.187.018-.324-1.978-.511-1.978l-.049-7.83 30.658-4.944 1.004-14.374c.203-2.91 2.551-5.263 5.463-5.472l38.551-2.75c.146-.01.29-.016.434-.016 2.897 0 5.401 2.166 5.825 5.038l1.959 13.286h28.005l1.959-13.286c.423-2.871 2.93-5.037 5.831-5.037.142 0 .284.005.423.015l38.556 2.75c2.911.209 5.26 2.562 5.463 5.472l1.003 14.374 30.645 4.966z" fill="#fff" transform="matrix(4.162611 0 0 -4.162611 919.24059 771.67186)"/><path d="m0 0v-47.514-6.035-5.492c.108-.001.216-.005.323-.015l36.196-3.49c1.896-.183 3.382-1.709 3.514-3.609l1.116-15.978 31.574-2.253 2.175 14.747c.282 1.912 1.922 3.329 3.856 3.329h38.188c1.933 0 3.573-1.417 3.855-3.329l2.175-14.747 31.575 2.253 1.115 15.978c.133 1.9 1.618 3.425 3.514 3.609l36.182 3.49c.107.01.214.014.322.015v4.711l.015.005v54.325c5.09692 6.4164715 9.92323 13.494208 13.621 19.449-5.651 9.62-12.575 18.217-19.976 26.182-6.864-3.455-13.531-7.369-19.828-11.534-3.151 3.132-6.7 5.694-10.186 8.372-3.425 2.751-7.285 4.768-10.946 7.118 1.09 8.117 1.629 16.108 1.846 24.448-9.446 4.754-19.519 7.906-29.708 10.17-4.068-6.837-7.788-14.241-11.028-21.479-3.842.642-7.702.88-11.567.926v.006c-.027 0-.052-.006-.075-.006-.024 0-.049.006-.073.006v-.006c-3.872-.046-7.729-.284-11.572-.926-3.238 7.238-6.956 14.642-11.03 21.479-10.184-2.264-20.258-5.416-29.703-10.17.216-8.34.755-16.331 1.848-24.448-3.668-2.35-7.523-4.367-10.949-7.118-3.481-2.678-7.036-5.24-10.188-8.372-6.297 4.165-12.962 8.079-19.828 11.534-7.401-7.965-14.321-16.562-19.974-26.182 4.4426579-6.973692 9.2079702-13.9828876 13.621-19.449z" fill="#478cbf" transform="matrix(4.162611 0 0 -4.162611 104.69892 525.90697)"/><path d="m0 0-1.121-16.063c-.135-1.936-1.675-3.477-3.611-3.616l-38.555-2.751c-.094-.007-.188-.01-.281-.01-1.916 0-3.569 1.406-3.852 3.33l-2.211 14.994h-31.459l-2.211-14.994c-.297-2.018-2.101-3.469-4.133-3.32l-38.555 2.751c-1.936.139-3.476 1.68-3.611 3.616l-1.121 16.063-32.547 3.138c.015-3.498.06-7.33.06-8.093 0-34.374 43.605-50.896 97.781-51.086h.066.067c54.176.19 97.766 16.712 97.766 51.086 0 .777.047 4.593.063 8.093z" fill="#478cbf" transform="matrix(4.162611 0 0 -4.162611 784.07144 817.24284)"/><path d="m0 0c0-12.052-9.765-21.815-21.813-21.815-12.042 0-21.81 9.763-21.81 21.815 0 12.044 9.768 21.802 21.81 21.802 12.048 0 21.813-9.758 21.813-21.802" fill="#fff" transform="matrix(4.162611 0 0 -4.162611 389.21484 625.67104)"/><path d="m0 0c0-7.994-6.479-14.473-14.479-14.473-7.996 0-14.479 6.479-14.479 14.473s6.483 14.479 14.479 14.479c8 0 14.479-6.485 14.479-14.479" fill="#414042" transform="matrix(4.162611 0 0 -4.162611 367.36686 631.05679)"/><path d="m0 0c-3.878 0-7.021 2.858-7.021 6.381v20.081c0 3.52 3.143 6.381 7.021 6.381s7.028-2.861 7.028-6.381v-20.081c0-3.523-3.15-6.381-7.028-6.381" fill="#fff" transform="matrix(4.162611 0 0 -4.162611 511.99336 724.73954)"/><path d="m0 0c0-12.052 9.765-21.815 21.815-21.815 12.041 0 21.808 9.763 21.808 21.815 0 12.044-9.767 21.802-21.808 21.802-12.05 0-21.815-9.758-21.815-21.802" fill="#fff" transform="matrix(4.162611 0 0 -4.162611 634.78706 625.67104)"/><path d="m0 0c0-7.994 6.477-14.473 14.471-14.473 8.002 0 14.479 6.479 14.479 14.473s-6.477 14.479-14.479 14.479c-7.994 0-14.471-6.485-14.471-14.479" fill="#414042" transform="matrix(4.162611 0 0 -4.162611 656.64056 631.05679)"/></g></svg>
A=   ���r�k5   res://Scenes/Main.tscnQ�3�|c   res://icon.svgWf��?�?   res://Scenes/Triangle_Line.tscntaߐ�6�   res://Scenes/Site_Line.tscn::�u��I?E�J���ECFG      application/config/name         voronoi_example    application/run/main_scene          res://Scenes/Main.tscn     application/config/features$   "         4.0    Forward Plus       application/config/icon         res://icon.svg  2   rendering/environment/defaults/default_clear_color        �?  �?  �?  �?��n