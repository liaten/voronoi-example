extends Node2D
var voronoi:Delaunay_Bower
var triangulars:Array
var current_state:STATE = STATE.START
var screen_size_x: float = DisplayServer.screen_get_size().x
var screen_size_y: float = DisplayServer.screen_get_size().y
var ranges:Dictionary = {
	"map_range":20
}
enum STATE{
	START,
	TRIANGULATE,
	VORONOI
}

# Called when the node enters the scene tree for the first time.
func _ready() ->void:
	$Button.text = "Триангулировать"
	current_state = STATE.START
	voronoi = Delaunay_Bower.new()
	
	for _i in range(ranges.map_range):
		voronoi.add_point(
			Vector2(
				randf_range(0,screen_size_x),
				randf_range(0,screen_size_y)))
	
	voronoi.sort_points()
	show_points()


func show_points() ->void:
	for point in voronoi.points:
		show_centroid(point)


func show_centroid(point: Vector2) ->void:
	var polygon = Polygon2D.new()
	var p = PackedVector2Array()
	var s = 4
	p.append(Vector2(-s,s))
	p.append(Vector2(s,s))
	p.append(Vector2(s,-s))
	p.append(Vector2(-s,-s))
	polygon.polygon = p
	polygon.color = Color.BLACK
	polygon.position = point
	add_child(polygon)


func show_site(site: Delaunay_Bower.VoronoiSite):
	var line = Site_Line.new()
	var p = site.polygon
	p.append(p[0])
	line.points = p
	line.width = 3
	line.default_color = Color.RED
	add_child(line)


func show_triangle(t: Delaunay_Bower.Triangle):
	var line = Triangle_Line.new()
	line.points = [t.a,t.b,t.c,t.a]
	line.width = 3
	line.default_color = Color.RED
	add_child(line)


func recolor_triangle_lines():
	for child in get_children():
		if child is Triangle_Line:
			child.width = 1
			child.default_color = Color.BLACK


func remove_points() ->void:
	for child in get_children():
		if child is Polygon2D:
			child.queue_free()


func remove_triangles() ->void:
	for child in get_children():
		if child is Triangle_Line:
			child.queue_free()


func remove_sites() ->void:
	for child in get_children():
		if child is Site_Line:
			child.queue_free()


func triangulate() ->void:
	triangulars = voronoi.triangulate()
	for t in triangulars:
		show_triangle(t)
	$Button.text = "Построить диаграмму Вороного"
	current_state = STATE.TRIANGULATE


func make_voronoi() ->void:
	var v: Dictionary = voronoi.make_voronoi(triangulars)
	for site_i in range(v.size()):
		var site = v[site_i] as Delaunay_Bower.VoronoiSite
		recolor_triangle_lines()
		show_site(site)
	$Button.text = "Сгенерировать новые точки"
	current_state = STATE.VORONOI


func _on_button_pressed() ->void:
	match current_state:
		STATE.START:
			triangulate()
			$LineEdit.hide()
			return
		STATE.TRIANGULATE:
			make_voronoi()
			$LineEdit.hide()
			return
		STATE.VORONOI:
			remove_points()
			remove_triangles()
			remove_sites()
			_ready()
			$LineEdit.text = str(ranges.map_range)
			$LineEdit.show()
			return


func _on_line_edit_text_changed(new_text):
	if(current_state == STATE.START):
		var range = int(new_text)
		if(range>2 && range < 500):
			ranges.map_range = range
			remove_points()
			_ready()
